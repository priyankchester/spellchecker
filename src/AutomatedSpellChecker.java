import java.io.*;
import java.nio.charset.Charset;

public class AutomatedSpellChecker {
    private static final String DICTIONARY_PATH = "/usr/share/dict/words";

    public static void loadWordsIntoDictionary(SpellChecker spellChecker){
        try (
                InputStream fis = new FileInputStream(DICTIONARY_PATH);
                BufferedReader dictReader = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));
        ) {
            String word;
            while ((word = dictReader.readLine()) != null) {
                spellChecker.addWordToDictionary(word);
            }
        } catch (IOException e){
            System.out.println("Error loading dictionary words from " + DICTIONARY_PATH);
        }
    }

    public static void main(String[] args){
        SpellChecker checker = new SpellChecker();
        loadWordsIntoDictionary(checker);
        for(String word: args){
            System.out.println(checker.checkSpelling(word));
        }
    }
}
