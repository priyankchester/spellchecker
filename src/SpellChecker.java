import java.util.*;

public class SpellChecker {

    private final String NO_SUGGESTION_TEXT = "NO SUGGESTION";

    Set<String> dictionary = new HashSet();

    public SpellChecker(){}

    public SpellChecker(Iterable<String> words){
        for(String word: words){
            addWordToDictionary(word);
        }
    }

    public void addWordToDictionary(String word){
        dictionary.add(word.toLowerCase());
    }

    public boolean contains(String word){
        return dictionary.contains(word.toLowerCase());
    }

    public String checkSpelling(String input){
        String word = input.toLowerCase();
        if(contains(word)){
            return word;
        }
        for(String vowelCombination: generateVowelCombinations(word)) {
            for (String repeatCombination : generateRepeatCombinations(vowelCombination)) {
                if (contains(repeatCombination)) {
                    return repeatCombination;
                }
            }
        }
        return(NO_SUGGESTION_TEXT);
    }

    public String checkVowelCombinations(String word){

        Optional<String> matchingCombination =
                generateVowelCombinations(word)
                        .stream()
                        .filter(c -> contains(c))
                        .findFirst();

        return matchingCombination.orElse("");
    }

    public List<String> generateVowelCombinations(String word){
        return generateVowelCombinations(word, 0, Arrays.asList(""));
    }

    private List<String> generateVowelCombinations(String word, int index, List<String> acc){

        if(index == word.length()){
           return acc;
        }

        List<String> newAcc = new ArrayList<String>();
        if(Vowel.isVowel(word.charAt(index))){
            for(String accValue: acc){
                for(Character vowel: Vowel.getVowels()){
                    newAcc.add(accValue + vowel);
                }
            }
        } else {
            for(String accValue: acc) {
                newAcc.add(accValue + word.charAt(index));
            }
        }

        return generateVowelCombinations(word, index + 1, newAcc);
    }

    public String checkRepeatCombinations(String word){
        List<String> combinations = generateRepeatCombinations(word);
        for(String combination: combinations){
            if(contains(combination)){
                return combination;
            }
        }
        return "";
    }

    public List<String> generateRepeatCombinations(String word){
        return generateRepeatCombinations(word, 0, Arrays.asList(""));
    }

    private List<String> generateRepeatCombinations(String word, int startIndex, List<String> acc) {

        if(startIndex == word.length()){
            return acc;
        }

        List<String> newAcc = new ArrayList<>();
        if(startIndex == word.length()-1){
            for(String ac: acc){
                newAcc.add(ac + word.charAt(startIndex));
            }
            return newAcc;
        } else if(startIndex < word.length()-1 && word.charAt(startIndex) == word.charAt(startIndex+1)){
            int endIndex = startIndex+1;
            while(endIndex < word.length() && word.charAt(startIndex) == word.charAt(endIndex)){
                endIndex++;
            }
            for(String ac: acc){
                for(int endIndexCopy = endIndex; endIndexCopy > startIndex; endIndexCopy--){
                    newAcc.add(ac + word.substring(startIndex, endIndexCopy));
                }
            }
            return generateRepeatCombinations(word, endIndex, newAcc);
        } else {
            int endIndex = startIndex+1;
            for(String ac: acc){
                newAcc.add(ac + word.substring(startIndex, endIndex));
            }
            return generateRepeatCombinations(word, endIndex, newAcc);
        }
    }
}
