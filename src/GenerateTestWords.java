import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenerateTestWords {

    private static final int TESTCASES = 5;

    private static final String DICTIONARY_PATH = "/usr/share/dict/words";

    public static List<String> getWordsFromDictionary(){
        List<String> result = new ArrayList<>(TESTCASES);

        Random random = new Random();

        try (
                InputStream fis = new FileInputStream(DICTIONARY_PATH);
                BufferedReader dictReader = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));
        ) {
            String word;
            while ((word = dictReader.readLine()) != null && result.size() < TESTCASES) {
                if(random.nextInt(10) == 0){
                    result.add(word);
                }
            }
        } catch (IOException e){
            System.out.println("Error loading dictionary words from " + DICTIONARY_PATH);
        }
        return result;
    }

    public static void main(String[] args){
        SpellChecker sc = new SpellChecker();
        for(String word: getWordsFromDictionary()){
            System.out.println(word);
            for(String vowelCombination: sc.generateVowelCombinations(word)){
                for(String repeatedCombination: sc.generateRepeatCombinations(vowelCombination)){
                    System.out.println(repeatedCombination);
                }
            }
        }
    }
}
