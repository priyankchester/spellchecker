import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Vowel {
    private static final String vowels = "aeiou";
    private static final Set<Character> vowelsSet = new HashSet(Arrays.asList('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'));

    public static boolean isVowel(Character c){
        return vowelsSet.contains(c);
    }

    public static char[] getVowels(){
        return vowels.toCharArray();
    }

}
