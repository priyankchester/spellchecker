import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class SpellCheckerTest {

    String[] words;

    @Before
    public void setUp() throws Exception {
        words = new String[]{"apple", "banana", "cherry", "conspiracy"};
    }

    @Test
    public void testContains() throws Exception {
        SpellChecker spellChecker = new SpellChecker(Arrays.asList(words));
        assertTrue(spellChecker.contains("apple"));
        assertTrue(spellChecker.contains("banana"));
        assertTrue(spellChecker.contains("cherry"));
        assertTrue(spellChecker.contains("Cherry"));
        assertFalse(spellChecker.contains(""));
        assertFalse(spellChecker.contains("appla"));
    }

    @Test
    public void testCheckSpelling() throws Exception {
        SpellChecker spellChecker = new SpellChecker(Arrays.asList(words));
        assertEquals("apple", spellChecker.checkSpelling("Apple"));
        assertEquals("banana", spellChecker.checkSpelling("baNANA"));
        assertEquals("apple", spellChecker.checkSpelling("appple"));
    }


    @Test
    public void testCheckForVowelCombinations() throws Exception {
        SpellChecker spellChecker = new SpellChecker(Arrays.asList(words));
        assertEquals("apple", spellChecker.checkVowelCombinations("appla"));
        assertEquals("apple", spellChecker.checkVowelCombinations("applo"));
        assertEquals("apple", spellChecker.checkVowelCombinations("appli"));
        assertEquals("", spellChecker.checkVowelCombinations("apply"));
    }

    @Test
    public void testRepeatCombinations() throws Exception {
        SpellChecker spellChecker = new SpellChecker(Arrays.asList(words));
        assertEquals("apple", spellChecker.checkRepeatCombinations("aaapppppllleee"));
        assertEquals("banana", spellChecker.checkRepeatCombinations("baaannaanaaaaa"));
        assertEquals("", spellChecker.checkRepeatCombinations("baaannaanaaanaa"));
    }

    @Test
    public void testVowelAndRepeatCombinations() throws Exception {
        SpellChecker spellChecker = new SpellChecker(Arrays.asList(words));
        assertEquals("apple", spellChecker.checkSpelling("EEppppLlLloo"));

        assertEquals("apple", spellChecker.checkSpelling("eEOppppLlLloo"));

        assertEquals("conspiracy", spellChecker.checkSpelling("CUNsperrICY"));

    }

    @Test
    public void testGenerateVowelCombinations() throws Exception {
        SpellChecker spellChecker = new SpellChecker(Arrays.asList(words));
        List<String> vowelCombinations = spellChecker.generateVowelCombinations("apple");
        assertTrue(vowelCombinations.size() == 25);
        assertTrue(vowelCombinations.contains("apple"));
        assertTrue(vowelCombinations.contains("appli"));
        assertTrue(vowelCombinations.contains("appla"));
        assertTrue(vowelCombinations.contains("applo"));
        assertTrue(vowelCombinations.contains("applu"));
        assertTrue(vowelCombinations.contains("epple"));
        assertTrue(vowelCombinations.contains("eppli"));
        assertTrue(vowelCombinations.contains("eppla"));
        assertTrue(vowelCombinations.contains("epplo"));
        assertTrue(vowelCombinations.contains("epplu"));
        assertTrue(vowelCombinations.contains("opple"));
        assertTrue(vowelCombinations.contains("oppli"));
        assertTrue(vowelCombinations.contains("oppla"));
        assertTrue(vowelCombinations.contains("opplo"));
        assertTrue(vowelCombinations.contains("opplu"));
        assertTrue(vowelCombinations.contains("ipple"));
        assertTrue(vowelCombinations.contains("ippli"));
        assertTrue(vowelCombinations.contains("ippla"));
        assertTrue(vowelCombinations.contains("ipplo"));
        assertTrue(vowelCombinations.contains("ipplu"));
        assertTrue(vowelCombinations.contains("upple"));
        assertTrue(vowelCombinations.contains("uppli"));
        assertTrue(vowelCombinations.contains("uppla"));
        assertTrue(vowelCombinations.contains("upplo"));
        assertTrue(vowelCombinations.contains("upplu"));
    }

    @Test
    public void testGenerateRepeatCombinations() throws Exception {
        SpellChecker spellChecker = new SpellChecker(Arrays.asList(words));
        List<String> repeatCombinations = spellChecker.generateRepeatCombinations("aaabbccc");
        assertTrue(repeatCombinations.size() == 18);
        assertTrue(repeatCombinations.contains("aaabbccc"));
        assertTrue(repeatCombinations.contains("aaabbcc"));
        assertTrue(repeatCombinations.contains("aaabbc"));
        assertTrue(repeatCombinations.contains("aaabccc"));
        assertTrue(repeatCombinations.contains("aaabcc"));
        assertTrue(repeatCombinations.contains("aaabc"));

        assertTrue(repeatCombinations.contains("aabbccc"));
        assertTrue(repeatCombinations.contains("aabbcc"));
        assertTrue(repeatCombinations.contains("aabbc"));
        assertTrue(repeatCombinations.contains("aabccc"));
        assertTrue(repeatCombinations.contains("aabcc"));
        assertTrue(repeatCombinations.contains("aabc"));

        assertTrue(repeatCombinations.contains("abbccc"));
        assertTrue(repeatCombinations.contains("abbcc"));
        assertTrue(repeatCombinations.contains("abbc"));
        assertTrue(repeatCombinations.contains("abccc"));
        assertTrue(repeatCombinations.contains("abcc"));
        assertTrue(repeatCombinations.contains("abc"));

    }

}